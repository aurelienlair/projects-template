# Project template

## Description

This project is <REPLACE_ME>

## Getting Started

The following instructions will allow you to set the project up from scratch.

## System requirements

<REPLACE_ME>

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of Python run command
```

| Type | Description |
|------| ----------- |
| `style` | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build` | Changes to the build process |
| `chore` | Changes to the build process or auxiliary tools and libraries such as documentation generation |
| `docs` | Documentation updates |
| `feat` | New features |
| `fix`  | Bug fixes |
| `refactor` | Code refactoring |
| `test` | Adding missing tests |
| `perf` | A code change that improves performance |

## Useful links

<REPLACE_ME>
